package main

import (
    "context"
    "os"
    "fmt"
    "net/http"
    "github.com/go-redis/redis/v8"
)

var rdb *redis.Client
var ctx = context.Background()

// declare the essentials and make redis connections

func init() {

     // declare the essentials and make redis connections
   var host = "localhost"
   var port = "6379"
 
   // get credentials from the Environment Variables
   if os.Getenv("REDIS_HOST") != "" {
       host = os.Getenv("REDIS_HOST")
   }
   if string(os.Getenv("REDIS_PORT")) != "" {
       port = string(os.Getenv("REDIS_PORT"))
   }

    rdb = redis.NewClient(&redis.Options{
          Addr:     host + ":" + port,
        //Addr: "localhost:6379", // Redis server address
    })
}


func hello(w http.ResponseWriter, r *http.Request) {
    incr := rdb.Incr(ctx, "counter")
    count, err := incr.Result()
    if err != nil {
        http.Error(w, "Error incrementing counter", http.StatusInternalServerError)
        return
    }
    fmt.Fprintf(w, "hello, my name is Falcon. You've visited %d times!", count)
}

func main() {
    http.HandleFunc("/", hello)
    port := ":4000"
    fmt.Println("Server running at http://localhost" + port)
    http.ListenAndServe(port, nil)
}
